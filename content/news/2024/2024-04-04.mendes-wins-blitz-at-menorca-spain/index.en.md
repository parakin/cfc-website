+++
title = "Aaron Reeve Mendes: 1st in Blitz at Menorca, Spain"
author = "Christina Tao, CFC Youth Coordinator"
+++

## 11-Year-Old Prodigy Aaron Mendes of Canada, Claims Victory in III Open International Chess Menorca BLITZ

In an astonishing turn of events at the
III Open International Chess Menorca BLITZ held on April 4th in Menorca, Spain,
11-year-old Aaron Reeve Mendes emerged as the champion.
Despite facing off against seasoned Grandmasters (GMs) and International Masters (IMs),
Aaron showcased unparalleled skill and strategic acumen, securing a remarkable 8 out of 9 wins
in the blitz tournament. More than 160 players around the world partook in this event.

Aarons' triumph has not only captivated the chess world but has
also solidified his position as a rising star in competitive chess.
Despite his age, Mendes displayed a level of maturity and precision
that surpassed expectations, navigating each match with finesse and confidence.
After this accomplishment, Aarons's blitz live ratings reached 2385,
the highest for any U12-year-old in the world.

See [chess-results.com](https://chess-results.com/tnr892600.aspx?lan=1&art=1&rd=9)

The young champion attributes much of his success to the support extended
by the Canadian Chess Federation along with training by FIDE, Chessable Camp
held in Spain from March 28 to April 1.

During the camp, Aaron had the privilege of experiencing the exceptional training methods
of GM Judith Polgar, combined with the expertise of GM Arthur Jussupow.
Aaron credits this intensive training regimen for sharpening his skills and preparing him
for the challenges of high-level competition.

Expressing gratitude, Mendes acknowledges the unwavering support
of the Canadian Chess Federation and St. Barbara Elementary School, Mississauga
throughout his chess journey.
Their encouragement and assistance have played a pivotal role in his development
as a chess player, enabling him to reach new heights in the realm of competitive chess.

Aarons' victory serves as an inspiration not only to young chess enthusiasts
but also to players of all ages, demonstrating that determination, hard work, support
and a passion for the game can lead to extraordinary achievements.
As Aaron continues to hone his craft and compete on the global stage,
he undoubtedly remains a Canadian player to watch in the world of chess.

{{< figure src="aaron.1.jpg" caption="Aaron in Round #1. First of 8 wins in a row." >}}
{{< figure src="aaron.3.jpg" >}}
{{< figure src="aaron.4.jpg" caption="Aaron on Board #1 for 5 of 9 rounds">}}
{{< figure src="aaron.5.jpg" caption="Aaron awarded 1st Place, Blitz">}}
{{< figure src="aaron.6.jpg" caption="Aaron with Chief Arbiter,IA Jose F. Suárez Roa" >}}
