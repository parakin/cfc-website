+++
title = "Arbiter Titres"
layout = "ws-single"
tableofcontents = false
+++

<div style="background-color:#FFFFCC; padding:0.5rem; border: 2px solid #ccc;">

Voir la [Base de données des arbitres FIDE](https://arbiters.fide.com/arbiters/arbiters-database)
<br>pour la dernière liste officielle des International Arbiter (IA),
FIDE Arbiter (FA), et National Arbiter (NA).

</div>
