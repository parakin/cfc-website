+++
title = "CFC Championships - Bids"
layout = "ws-single"
content_review = "2024-05-01"
+++

## 2024
### Canadian Closed CC & Zonal
* Awarded: Mar 27-Apr 2 in Toronto (Hart House), ON ([website](https://harthousechess.com/2024-canadian-zonals/))
### Canadian Women's Closed CC & Zonal
* Awarded: Mar 28-Apr 2 in Toronto (Hart House), ON ([website](https://harthousechess.com/2024-canadian-zonals/))
### Canadian Open CC
* Awarded: July 13-18 in Laval, QC ([website](https://cycc2024.my.canva.site/))
### Canadian Youth CC (CYCC)
* Awarded: July 8-12 in Laval, QC ([website](https://cycc2024.my.canva.site/))
### Canadian Junior CC
* Awarded: April 10-14 in Etobicoke, ON ([website](https://excelsiorchessclub.ca/our-tournaments/2024-canadian-juniors/))
### Canadian Senior CC
* Awarded: Aug 3-5 in Calgary, AB

## 2025
### Canadian Closed CC
* Awarded: April 17-22 in Toronto, ON ([Annex CC](https://annexchessclub.com/))
### Canadian Women's Closed CC
* Awarded: April 17-22 in Toronto, ON ([Annex CC](https://annexchessclub.com/))
### Canadian Open CC
* Awarded: July 12-18 in Surrey, BC (Vancity & Juniors to Masters)
### Canadian Youth CC (CYCC)
* Awarded: July 8-11 in Surrey, BC (Vancity & Juniors to Masters)
### Canadian Junior CC
* Bids: (none)
### Canadian Senior CC
* Bids: (none)


## 2026
### Canadian Closed CC & Zonal
* Bids: (none)
### Canadian Women's Closed CC & Zonal
* Bids: (none)
### Canadian Open CC
* Bids: (none)
### Canadian Youth CC (CYCC)
* Bids: (none)
### Canadian Junior CC
* Bids: (none)
### Canadian Senior CC
* Bids: (none)
