+++
title = "Championnats FCE - Offres"
layout = "ws-single"
+++

## 2024
### Canadian Closed CC & Zonal
* Attribué: Mar 27-Apr 2 en Toronto (Hart House), ON ([website](https://harthousechess.com/2024-canadian-zonals/))
### Canadian Women's Closed CC & Zonal
* Attribué: Mar 28-Apr 2 en Toronto (Hart House), ON ([website](https://harthousechess.com/2024-canadian-zonals/))
### Canadian Open CC
* Attribué: July 13-18 en Laval, QC ([website](https://cycc2024.my.canva.site/))
### Canadian Youth CC (CYCC)
* Attribué: July 8-12 en Laval, QC ([website](https://cycc2024.my.canva.site/))
### Canadian Junior CC
* Attribué: April 10-14 en Etobicoke, ON ([website](https://excelsiorchessclub.ca/our-tournaments/2024-canadian-juniors/))
### Canadian Senior CC
* Attribué: Aug 3-5 en Calgary, AB

## 2025
### Canadian Closed CC
* Attribué: April 17-22 in Toronto, ON ([Annex CC](https://annexchessclub.com/))
### Canadian Women's Closed CC
* Attribué: April 17-22 in Toronto, ON ([Annex CC](https://annexchessclub.com/))
### Canadian Open CC
* Attribué: July 12-18 en Surrey, BC (Vancity & Juniors to Masters)
### Canadian Youth CC (CYCC)
* Attribué: July 8-11 en Surrey, BC (Vancity & Juniors to Masters)
### Canadian Junior CC
* Offres: (aucune)
### Canadian Senior CC
* Offres: (aucune)


## 2026
### Canadian Closed CC & Zonal
* Offres: (aucune)
### Canadian Women's Closed CC & Zonal
* Offres: (aucune)
### Canadian Open CC
* Offres: (aucune)
### Canadian Youth CC (CYCC)
* Offres: (aucune)
### Canadian Junior CC
* Offres: (aucune)
### Canadian Senior CC
* Offres: (aucune)
