+++
title = "Rating Fees"
layout = "ws-single"
tableofcontents = false
+++

## CFC Rating Fees
 
Regular tournaments (time control of &ge; 60 minutes for a 60 move game):
* $3.00+tax/player if results are submitted in CFC's file format (.ctr and .tms files) 
  <br>$5.00+tax/player otherwise.

Quick tournaments (time control of &lt; 60 minutes for a 60 move game (and &ge; 5 min)):
* $1.50+tax/player if results are submitted in CFC's file format (.ctr and .tms files) 
  <br>$3.50+tax/player otherwise.

Junior tournaments (Regular or Quick)
* $0.50+tax/player if results are submitted in CFC's file format (.ctr and .tms files)
  <br>$2.50+tax/player otherwise.
* Note: For CFC membership dues (which are different fees), if all players are juniors,
  CFC membership dues are not required.  However, BC tournament fees *are* required; see next.

British Columbia tournament fees
* Instead of annual BC Chess Federation membership dues, CFC-rated tournaments held in BC
  are required to pay an extra fee per player along with the CFC rating fees.
  These fees are paid to the CFC (who collect them on behalf of the BCCF).
  See [BCCF Membership](https://www.chess.bc.ca/membership.php).
* Fees are per player for both BC residents and non-residents
  but not for BCCF Life Members ([list](https://www.chess.bc.ca/lifemembers.php)).
* $3.00+tax/player for multi-day events and $1.50+tax/player for single day events.

Taxes: Add HST or GST+PST taxes according to the "Taxes" section below.

To be CFC-rated, the event organizer must send the total fees to the CFC along with the event's results.
Any CFC membership dues (new and renewals) collected at the event should be sent to the CFC too.
 
## FIDE Rating Fees

Notification and Reporting
* _Before the tournament_, the organizer must notify the CFC Office 
  that the event should be FIDE rated.
* The organizer must report the tournament results quickly.
* Failure to do so will result in an additional fee of $100.

For all events (Swiss System, Round Robin, matches):
* $2.20 + tax per player

Taxes: Add HST or GST+PST taxes according to the "Taxes" section below.
 
## Taxes on CFC and FIDE Rating Fees

As of July 1, 2011, HST or PST+GST taxes are applicable on CFC and FIDE rating fees.

| Province of Event | Taxes | HST | GST+PST |
|----------|-------|-----|---------|
| Alberta | 5% | - | 5+0% | 
| British Columbia | 12% | - | 5+7% |
| Manitoba | 12% | - | 5+7% |
| New Brunswick | 15% | 15% | - |
| Newfoundland and Labrador | 15% | 15% | - |
| Northwest Territories | 5% | - | 5+0% |
| Nova Scotia | 15% | 15% | - |
| Nunavut | 5% | - | 5+0% |
| Ontario | 13% | 13% | - |
| Quebec | 14.975% | - | 5+9.975% |
| Prince Edward Island | 15% | 15% | - |
| Saskatchewan | 11% | - | 5+6% |
| Yukon | 5% | - | 5+0% |
