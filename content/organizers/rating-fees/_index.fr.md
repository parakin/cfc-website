+++
title = "Frais de cote"
layout = "ws-single"
tableofcontents = false
+++

## Frais de cote FCE

Tournois réguliers (contrôle du temps &ge; 60 minutes pour une partie de 60 coups):
* 3,00$+taxes/joueur si les résultats sont soumis au format de fichier FCE (fichiers .ctr et .tms)
  <br>5,00$+taxes/joueur sinon.

Tournois rapides (contrôle du temps &lt; 60 minutes pour une partie de 60 coups (et &ge; 5 min)):
* 1,50$+taxes/joueur si les résultats sont soumis au format de fichier FCE (fichiers .ctr et .tms)
  <br>3,50$+taxes/joueur sinon.

Tournois juniors (réguliers ou rapides)
* 0,50$+taxes/joueur si les résultats sont soumis au format de fichier FCE (fichiers .ctr et .tms)
  <br>2,50$+taxes/joueur sinon.
* Remarque: Pour les cotisations d'adhésion au FCE (qui sont des frais différents),
  si tous les joueurs sont juniors, Les frais d'adhésion aux FCC ne sont pas exigés.
  Cependant, les frais de tournoi de la BC *sont* requis; voir ensuite.

Frais de tournoi en Colombie-Britannique
* Au lieu des cotisations annuelles des membres de la BC Chess Federation,
  tournois classés FCE organisés en Colombie-Britannique
  sont tenus de payer des frais supplémentaires par joueur en plus des frais de classement FCE.
  Ces frais sont versés au FCE (qui les perçoit pour le compte du BCCF).
  Voir [BCCF Membership](https://www.chess.bc.ca/membership.php).
* Les frais sont par joueur pour les résidents de la BC et les non-résidents
  mais pas pour les membres à vie de BCCF ([liste](https://www.chess.bc.ca/lifemembers.php)).
* 3,00$+taxes/joueur pour les événements de plusieurs jours
  et 1,50$+taxes/joueur pour les événements d'une journée.

Taxes: Ajoutez les taxes HST ou GST+PST selon la section «Taxes» ci-dessous.

Pour être classé FCE, l'organisateur de l'événement doit envoyer le total des frais au FCE
avec les résultats de l'événement.
Toutes les cotisations d'adhésion aux FCE (nouvelles et renouvellements) collectées
lors de l'événement doivent également être envoyées aux FCE.
 
## Frais de cote FIDE

Notification et rapport
* _Avant le tournoi_, l'organisateur doit aviser le bureau du FCE
  que l'événement devrait être classé par la FIDE.
* L'organisateur doit rapporter les résultats du tournoi rapidement.
* Ne pas le faire entraînera des frais supplémentaires de 100$.

Pour tous les événements (Système Suisse, Round Robin, matchs) :
* 2,20$+taxes par joueur

Taxes: Ajoutez les taxes HST ou GST+PST selon la section «Taxes» ci-dessous.

## Taxes sur les frais de notation CFC et FIDE

Depuis le 1er juillet 2011, les taxes HST ou GST+PST sont applicables sur les frais de notation FCE et FIDE.

| Province de l'événement | Taxes | HST | GST+PST |
|----------|---------|-----|---------|
| Alberta | 5%      | - | 5+0% | 
| British Columbia | 12%     | - | 5+7% |
| Manitoba | 12%     | - | 5+7% |
| New Brunswick | 15%     | 15% | - |
| Newfoundland and Labrador | 15%     | 15% | - |
| Northwest Territories | 5%      | - | 5+0% |
| Nova Scotia | 15%     | 15% | - |
| Nunavut | 5%      | - | 5+0% |
| Ontario | 13%     | 13% | - |
| Quebec | 14.975% | - | 5+9.975% |
| Prince Edward Island | 15%     | 15% | - |
| Saskatchewan | 11%     | - | 5+6% |
| Yukon | 5%      | - | 5+0% |
