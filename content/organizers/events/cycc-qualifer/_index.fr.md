+++
title = "Événements de qualification CYCC"
layout = "ws-single"
tableofcontents = true
+++

Le « Championnat canadien d'échecs pour les jeunes » ou CYCC est le plus grand
événement annuel d'échecs au Canada. En 2024, 678 joueurs ont participé.

Pour jouer au CYCC, un joueur doit se « qualifier ».
Une façon (la principale) de se qualifier est de participer à au moins un
tournoi d'échecs « CYCC Qualifier » et d'obtenir un score de 50 % ou plus.

## Organisateurs des épreuves de qualification du CYCC

* Tous les joueurs doivent être « U18 » (âgés de moins de 18 ans au 31 décembre,
  <span cfc-constant="previous-year">____</span>).
* Les événements doivent se dérouler en personne, par voie électronique.
  Les événements en ligne nécessitent l'approbation préalable du FCE.
* Les appariements d'événements peuvent utiliser les méthodes du système suisse ou du Round-Robin.
* Les dates de l'événement doivent être telles que les résultats du tournoi
  soient soumis au FCE avant le 1er juillet.
* Les événements doivent avoir des sections distinctes: pour chaque groupe d'âge, une section Open & Girls:
  * Si possible, U8O, U8G, U10O, U10G, U12O, U12G, U14O, U14G, U16O, U16G, U18O, U18G.
  * Si ce n'est pas possible, pour les événements plus petits, les sections peuvent être combinées
    pour avoir un nombre raisonnable de joueurs dans chaque section. Les sections féminines peuvent
    être combinées en sections ouvertes.
    Les groupes d'âge adjacents inférieurs peuvent être combinés en un groupe d'âge supérieur.
* Les événements doivent comporter au moins 4 tours.
* Les événements doivent avoir un contrôle de temps minimum de 25 minutes par joueur pour une partie de 60 coups.
  * Par exemple, 15 minutes plus un incrément de 10 secondes par mouvement sont valides. 
* Les événements sont classés FCE.
  * Tous les joueurs doivent avoir un identifiant FCE ([Rejoindre/Renouveler le FCE](/fr/players/membership-join/)
    pour en obtenir un).
  * Puisqu'il s'agit d'un événement réservé aux juniors, les adhésions payantes et actives au FCE ne sont pas requises.
  * FCE « régulier » classé si le contrôle du temps est &ge; 60 minutes pour 60 coups.
  * FCE « Quick » classé si le contrôle du temps est &lt; 60 minutes pour 60 coups.
* Les événements peuvent autoriser des exemptions à la discrétion de l'organisateur.
  * Nous suggérons d'autoriser jusqu'à 2 byes de 12 points dans les 3 premiers tours et 0 byes de 0 point pour tous les autres jusqu'à un maximum de 50 % du nombre de tours.
* Les frais suivants seront facturés aux événements lorsque les résultats du tournoi seront soumis au FCE pour l'évaluation et pour l'ajout de joueurs à la <span cfc-constant="current-year">____</span> liste des joueurs qualifiés du CYCC:
  * Contribution de 1,50 $ par joueur au Fonds jeunesse du FCE.
  * 0,50 $ + taxes par joueur [Frais de classement](/fr/organizers/rating-fees/).
* Respecter toute règle applicable dans le manuel du FCE, section 7, [Canadian Youth Chess Championship](https://www.chess.ca/en/cfc/rules/cfc-handbook-2014/#canadian-youth-chess-championship),
 
## Joueurs des épreuves de qualification du CYCC

* Pour vous qualifier pour participer au CYCC via un événement de qualification CYCC:
  * Vous devez jouer au moins 3 matchs lors de l'événement (sauf byes).
  * Votre pointage doit être de 50 % ou plus (y compris les byes).
  * Vous devez vérifier que votre nom a été ajouté à la liste des joueurs qualifiés du CYCC trouvée sur le [site Web du FCE](/fr/).