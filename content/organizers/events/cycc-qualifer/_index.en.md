+++
title = "CYCC Qualifier Events"
layout = "ws-single"
tableofcontents = true
+++

The "Canadian Youth Chess Championship" or CYCC
is the largest annual chess event in Canada.
In 2024, 678 players participated.

To play in the CYCC, a player must "qualify".
One way (the main way) to qualify is to play in
at least one "CYCC Qualifier" chess tournament and score 50% or more.

## Organizers of CYCC Qualifier Events

* All players must be "U18"
  (under the age of 18 on Dec 31, <span cfc-constant="previous-year">____</span>).
* Events should be in-person, over-the-board.
  Online events require prior approval by the CFC. 
* Event pairings may use the Swiss-System or Round-Robin methods.
* Event dates must be such that the tournament results will be submitted to the CFC
  by July 1.  For 2025, the CYCC organizer will accept results up to and including
  the July 2-4, 2025 ratings cycle.
* Events should have separate sections: for each age group, an Open & Girls section: 
  * If possible, U8O, U8G, U10O, U10G, U12O, U12G, U14O, U14G, U16O, U16G, U18O, U18G.
  * If not possible, for smaller events, sections may be combined to have a reasonable
    number of players in each section. Girls sections may be combined into Open sections.
    Lower adjacent age groups may be combined into a higher age group. 
* Events must have at least 4 rounds.
* Events must have a minimum time control of 25 minutes per player for a 60 move game.
    * For example, 15 minutes plus a 10 seconds per move increment is valid.
* Events are CFC-rated.
  * All players must have a CFC id
    ([Join/Renew the CFC](/en/players/membership-join/) to get one).
  * Since this is an all-junior event, active paid CFC memberships are not required.
  * CFC "Regular" rated if the time control is &ge; 60 minutes for 60 moves.
  * CFC "Quick" rated if the time control is &lt; 60 minutes for 60 moves.
* Events may allow byes at the discretion of the organizer.
  * We suggest allowing up to 2 &frac12;-point byes in the first 3 rounds
    and 0-point byes for all others to a maximum of 50% of the number of rounds.
* Events will be charged the following fees when the tournament's results are submitted
  to the CFC for rating and for adding players to the
  <span cfc-constant="current-year">____</span> CYCC Qualified Players list:
  * $1.50 per player contribution to the CFC Youth Fund.
  * $0.50+tax per player [Rating Fees](https://www.chess.ca/en/organizers/rating-fees/).
* Adhere to any applicable rule in the CFC Handbook, Section 7, 
  [Canadian Youth Chess Championship](https://www.chess.ca/en/cfc/rules/cfc-handbook-2014/#canadian-youth-chess-championship),

## Players of CYCC Qualifier Events

* To qualify to play in the CYCC via a CYCC Qualifier event:
  * You must play at least 3 games in the event (excluding byes).
  * Your score must be 50% or more (including byes).
  * You should verify your name has been added to the CYCC Qualified Players list
    found on the [CFC website](/en/). 
